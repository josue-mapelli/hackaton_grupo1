package com.techu.tarjetas.controlador;

import com.techu.tarjetas.modelos.Tarjeta;
import com.techu.tarjetas.servicios.ObjetoNoEncontrado;
import com.techu.tarjetas.servicios.ServicioTarjeta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping(Rutas.TARJETA)
public class ControladorTarjeta {

    @Autowired
    ServicioTarjeta servicioTarjeta;

    @GetMapping
    public List<Tarjeta> obtenerTarjetas(@RequestParam int pagina, @RequestParam int cantidad) {
        try {
            return this.servicioTarjeta.obtenerTarjetas(pagina - 1, cantidad);
        } catch (ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{pan}")
    public Tarjeta obtenerTarjeta(@PathVariable String pan) {
        try {
            System.out.println(String.format("obtenerTarjeta %s", pan));
            return this.servicioTarjeta.obtenerTarjeta(pan);
        } catch (ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public void agregarTarjeta(@RequestBody Tarjeta tarjeta) {
        System.out.println(String.format("agregarTarjeta"));
        this.servicioTarjeta.crearTarjeta(tarjeta);
    }

    @PatchMapping("/{pan}")
    public void emparcharTarjeta(@PathVariable("pan") String pan,
                                 @RequestBody Tarjeta tarjeta) {
        System.out.println(String.format("emparcharTarjeta %s", pan));
        try {
            this.servicioTarjeta.emparcharTarjeta(pan, tarjeta);
        } catch (ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

}
