package com.techu.tarjetas.servicios;

public class ObjetoNoEncontrado extends RuntimeException {
    public ObjetoNoEncontrado(String message) {
        super(message);
    }
}
