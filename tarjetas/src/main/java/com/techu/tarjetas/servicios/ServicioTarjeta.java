package com.techu.tarjetas.servicios;

import com.techu.tarjetas.modelos.Tarjeta;

import java.util.List;

public interface ServicioTarjeta {

    List<Tarjeta> obtenerTarjetas(int pagina, int cantidad);

    void crearTarjeta(Tarjeta tarjeta);

    Tarjeta obtenerTarjeta(String pan);

    void emparcharTarjeta(String pan, Tarjeta tarjeta);
}
