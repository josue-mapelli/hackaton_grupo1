package com.techu.tarjetas.servicios.impl;

import com.techu.tarjetas.modelos.Tarjeta;
import com.techu.tarjetas.servicios.ObjetoNoEncontrado;
import com.techu.tarjetas.servicios.ServicioTarjeta;
import com.techu.tarjetas.servicios.repositorios.RepositorioTarjeta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioTarjetaImpl implements ServicioTarjeta {
    @Autowired
    RepositorioTarjeta repositorioTarjeta;

    @Override
    public List<Tarjeta> obtenerTarjetas(int pagina, int cantidad) {
        return this.repositorioTarjeta.findAll();
    }

    @Override
    public void crearTarjeta(Tarjeta tarjeta) {
        this.repositorioTarjeta.insert(tarjeta);
    }

    @Override
    public Tarjeta obtenerTarjeta(String pan) {
        final Optional<Tarjeta> quizasTarjeta = this.repositorioTarjeta.findById(pan);
        if (!quizasTarjeta.isPresent())
            throw new ObjetoNoEncontrado("No existe la tarjeta: " + pan);
        return quizasTarjeta.get();
    }

    @Override
    public void emparcharTarjeta(String pan, Tarjeta tarjeta) {
        Tarjeta target = this.obtenerTarjeta(pan);
        target.setFechaExpiracion(tarjeta.getFechaExpiracion());

        this.repositorioTarjeta.save(target);
    }
}
