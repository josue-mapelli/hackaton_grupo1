package com.techu.tarjetas.servicios.repositorios;

import com.techu.tarjetas.modelos.Tarjeta;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioTarjeta extends MongoRepository<Tarjeta,String> {
}
