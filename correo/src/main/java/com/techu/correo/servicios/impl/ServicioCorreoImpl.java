package com.techu.correo.servicios.impl;

import com.techu.correo.modelos.EstructuraCorreo;
import com.techu.correo.servicios.ServicioCorreo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.concurrent.CompletableFuture;

@Service
public class ServicioCorreoImpl implements ServicioCorreo {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServicioCorreoImpl.class);

    @Autowired
    private JavaMailSender sender;

    @Override
    @Async
    public CompletableFuture<Boolean> enviarEstructuraCorreo(EstructuraCorreo estructuraCorreo)  {
        LOGGER.info("enviarEstructuraCorreo");
        return CompletableFuture.completedFuture(enviarCorreo(estructuraCorreo.getDestinatario()
                , estructuraCorreo.getAsunto(), estructuraCorreo.getContenido()));
    }

    private boolean enviarCorreo(String destinatario,String asunto, String contenido) {
        boolean send = false;
        MimeMessage mensaje = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mensaje);
        try {
            helper.setTo(destinatario);
            helper.setText(contenido, true);
            helper.setSubject(asunto);
            sender.send(mensaje);
            send = true;
            LOGGER.info("Mail enviado!");
        } catch (MessagingException e) {
            LOGGER.error("Hubo un error al enviar el mail: {}", e);
        }
        return send;
    }
}