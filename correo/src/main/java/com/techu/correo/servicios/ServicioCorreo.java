package com.techu.correo.servicios;

import com.techu.correo.modelos.EstructuraCorreo;
import com.techu.correo.modelos.EstructuraCorreoClienteTarjeta;

import java.util.concurrent.CompletableFuture;

public interface ServicioCorreo {

    public CompletableFuture<Boolean> enviarEstructuraCorreo(EstructuraCorreo emailBody);
}
