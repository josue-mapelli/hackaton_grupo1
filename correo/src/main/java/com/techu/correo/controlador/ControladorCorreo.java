package com.techu.correo.controlador;

import com.techu.correo.modelos.EstructuraCorreo;
import com.techu.correo.servicios.ServicioCorreo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(Rutas.CORREO)
public class ControladorCorreo {

    @Autowired
    ServicioCorreo servicioCorreo;

    @PostMapping
    @ResponseBody
    public ResponseEntity enviarCorreo(@RequestBody EstructuraCorreo estructuraCorreo) {
        servicioCorreo.enviarEstructuraCorreo(estructuraCorreo);
        return ResponseEntity.ok().build();
    }

}
