package com.techu.clientes.modelos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document("clientes")
public class Cliente {
    //@JsonIgnore public String id;
    @Id
    public String documento;
    public String nombre;
    public String apellidos;
    public String edad;
    public String fechaNacimiento;
    public String telefono;
    public String correo;
    public String direccion;

    // Jackson - Serializacion/Deserializacion JSON
    @JsonIgnore
    public List<String> codigosCuentas = new ArrayList<>();
}
