package com.techu.clientes.modelos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document
public class ClienteTarjeta {
    //@JsonIgnore public String id;
    @Id public String pan;
    public String tipoTarjeta;
    public String fechaExpiracion;
    public String codOficina;
    public String activo;
    public String documento;


    // Jackson - Serializacion/Deserializacion JSON
    @JsonIgnore
    public List<String> codigosCuentas = new ArrayList<>();
}