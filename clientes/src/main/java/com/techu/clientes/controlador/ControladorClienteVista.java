package com.techu.clientes.controlador;

import com.techu.clientes.modelos.Cliente;
import com.techu.clientes.servicios.ObjetoNoEncontrado;
import com.techu.clientes.servicios.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;

@Controller
@RequestMapping(Rutas.CLIENTES)
public class ControladorClienteVista {

    @Autowired
    ServicioCliente servicioCliente;

    @GetMapping("/init")
    public String vistaCliente(Model model) {
        return "clientes";
    }

    @GetMapping("/init/todos")
    public String obtenerClientes(Model model) {
        model.addAttribute("listaClientes", servicioCliente.obtenerClientes(1,10));
        return "clientes :: resultsList";
    }

    @GetMapping("init/{documento}")
    public String obtenerUnCliente(Model model, @PathVariable("documento") String documento) {
        model.addAttribute("listaClientes", servicioCliente.obtenerCliente(documento));

        return "clientes :: resultsList";
    }


}
