package com.techu.clientes.controlador;

import com.techu.clientes.modelos.ClienteTarjeta;
import com.techu.clientes.servicios.ObjetoNoEncontrado;
import com.techu.clientes.servicios.ServicioCliente;
import com.techu.clientes.servicios.ServicioClienteTarjeta;
import com.techu.clientes.utilitarios.CuerpoCorreo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

//@RestController
//@RequestMapping(Rutas.CLIENTESTARJETA)
public class ControladorClienteTarjeta {

    @Autowired
    ServicioClienteTarjeta servicioClienteTarjeta;

    @Autowired
    ServicioCliente servicioCliente;

    @Autowired
    RestTemplate restTemplate;

    // GET http://localhost:9000/api/v1/clientestarjeta?pagina=7&cantidad=3 -> List<Cliente> obtenerClientes()
    @GetMapping
    public List<ClienteTarjeta> obtenerClientesTarjeta(@RequestParam int pagina, @RequestParam int cantidad) {
        try {
            return this.servicioClienteTarjeta.obtenerClientesTarjeta(pagina - 1, cantidad);
        } catch (ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    // POST http://localhost:9000/api/v1/clientestarjeta + DATOS -> agregarCliente(Cliente)
    @PostMapping
    public void agregarClienteTarjeta(@RequestBody ClienteTarjeta clienteTarjeta) {
        this.servicioClienteTarjeta.insertarClienteTarjetaNuevo(clienteTarjeta);
    }

    // GET http://localhost:9000/api/v1/clientestarjeta/{documento} -> obtenerUnCliente(documento)
    // GET http://localhost:9000/api/v1/clientestarjeta/12345678 -> obtenerUnCliente("12345678")
    @GetMapping("/{documento}")
    public List<ClienteTarjeta> obtenerClienteTarjeta(@PathVariable String documento) {
        try {
            System.err.println(String.format("obtenerUnCliente %s", documento));
            return this.servicioClienteTarjeta.obtenerClienteTarjeta(documento);
        } catch (ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    // PUT http://localhost:9000/api/v1/clientestarjeta/{documento} + DATOS -> emparcharUnCliente(documento, DATOS)
    // PUT http://localhost:9000/api/v1/clientestarjeta/12345678 + DATOS -> emparcharUnCliente("12345678", DATOS)
    @PatchMapping("/{documento}")
    public void emparacharUnClienteTarjeta(@PathVariable("documento") String nroDocumento,
                                           @RequestBody ClienteTarjeta clienteTarjeta) {
        try {
            clienteTarjeta.documento = nroDocumento;
            clienteTarjeta = this.servicioClienteTarjeta.emparcharClienteTarjeta(clienteTarjeta);

        } catch (ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    private String actualizarContenidoCorreo(String nombre, String tarjeta, String fecha) {
        String contenido = CuerpoCorreo.cuerpo;
        contenido = contenido.replace(CuerpoCorreo.nombre, nombre);
        contenido = contenido.replace(CuerpoCorreo.nroTarjeta, tarjeta);
        contenido = contenido.replace(CuerpoCorreo.fechaExpiracion, fecha);

        return contenido;
    }
}
