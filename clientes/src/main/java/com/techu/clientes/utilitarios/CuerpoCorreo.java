package com.techu.clientes.utilitarios;

public class CuerpoCorreo {

    public static final String asuntoCorreo = "Notificación tarjeta BBVA";
    public static final String nombre = "{nombre}";
    public static final String nroTarjeta = "{nroTarjeta}";
    public static final String fechaExpiracion = "{fechaExpiracion}";

    public static final String cuerpo = "<h3>\n" +
            "Estimado " + nombre + "</h3>\n" +
            "\n" +
            "Para informarle que la fecha de renovación de su tarjeta " + nroTarjeta + " se realizó con éxito.\n" +
            "<br>\n" +
            "<br>\n" +
            "La nueva fecha de expiración es: " + fechaExpiracion + "\n" +
            "\n" +
            "<br>\n" +
            "<br>\n" +
            "Saludos,\n" +
            "<br>\n" +
            "Equipo Nro #1";
}
