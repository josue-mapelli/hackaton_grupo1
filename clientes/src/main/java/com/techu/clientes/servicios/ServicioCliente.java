package com.techu.clientes.servicios;

import com.techu.clientes.modelos.Cliente;

import java.util.List;

public interface ServicioCliente {

    // CRUD

    public List<Cliente> obtenerClientes(int pagina, int cantidad);

    // CREATE
    public void insertarClienteNuevo(Cliente cliente);

    // READ
    public Cliente obtenerCliente(String documento);

    // UPDATE (solamente modificar, no crear).
    public void guardarCliente(Cliente cliente);

}