package com.techu.clientes.servicios.impl;

import com.techu.clientes.modelos.Cliente;
import com.techu.clientes.servicios.ObjetoNoEncontrado;
import com.techu.clientes.servicios.ServicioCliente;
import com.techu.clientes.servicios.repositorios.RepositorioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioClienteImpl implements ServicioCliente {

    @Autowired
    RepositorioCliente repositorioCliente;

    @Override
    public List<Cliente> obtenerClientes(int pagina, int cantidad) {
        return this.repositorioCliente.findAll();
    }

    @Override
    public void insertarClienteNuevo(Cliente cliente) {
        this.repositorioCliente.insert(cliente);
    }

    @Override
    public Cliente obtenerCliente(String documento) {
        final Optional<Cliente> quizasCliente = this.repositorioCliente.findById(documento);
        if (!quizasCliente.isPresent())
            throw new ObjetoNoEncontrado("No existe el cliente con documento " + documento);
        return quizasCliente.get();
    }

    @Override
    public void guardarCliente(Cliente cliente) {
        if (!this.repositorioCliente.existsById(cliente.documento))
            throw new ObjetoNoEncontrado("No existe el cliente con documento " + cliente.documento);
        this.repositorioCliente.save(cliente);
    }

}
