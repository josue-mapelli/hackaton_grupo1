package com.techu.clientes.servicios.impl;

import com.techu.clientes.modelos.Cliente;
import com.techu.clientes.modelos.ClienteTarjeta;
import com.techu.clientes.servicios.ObjetoNoEncontrado;
import com.techu.clientes.servicios.ServicioClienteTarjeta;
import com.techu.clientes.servicios.repositorios.RepositorioClienteTarjeta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioClienteTarjetaImpl implements ServicioClienteTarjeta {

    @Autowired
    RepositorioClienteTarjeta repositorioClienteTarjeta;

    @Override
    public List<ClienteTarjeta> obtenerClientesTarjeta (int pagina, int cantidad) {
        return this.repositorioClienteTarjeta.findAll();
    }

    @Override
    public void insertarClienteTarjetaNuevo(ClienteTarjeta clienteTarjeta) {
        this.repositorioClienteTarjeta.insert(clienteTarjeta);
    }

    @Override
    public List<ClienteTarjeta> obtenerClienteTarjeta(String documento) {
        final List<ClienteTarjeta> listaClienteTarjeta = this.repositorioClienteTarjeta.findByDocumento(documento);
        if (listaClienteTarjeta.isEmpty())
            throw new ObjetoNoEncontrado("No existe tarjeta del cliente con documento: " + documento);
        return listaClienteTarjeta;
    }

    @Override
    public ClienteTarjeta emparcharClienteTarjeta(ClienteTarjeta parche) {

        final Optional<ClienteTarjeta> clienteTarjeta = this.repositorioClienteTarjeta.findById(parche.pan);

        if (!clienteTarjeta.isPresent())
            throw new ObjetoNoEncontrado("No existe tarjeta del cliente: " + parche.pan);

        if (parche.fechaExpiracion.isEmpty())
            throw new ObjetoNoEncontrado("La fecha de expiracion no puede ser vacia");

        clienteTarjeta.get().fechaExpiracion = parche.fechaExpiracion;
        this.repositorioClienteTarjeta.save(clienteTarjeta.get());

        return clienteTarjeta.get();
    }

}
