package com.techu.clientes.servicios;

import com.techu.clientes.modelos.Cliente;
import com.techu.clientes.modelos.ClienteTarjeta;

import java.util.List;

public interface ServicioClienteTarjeta {

    // CRUD

    public List<ClienteTarjeta> obtenerClientesTarjeta(int pagina, int cantidad);

    // CREATE
    public void insertarClienteTarjetaNuevo(ClienteTarjeta clienteTarjeta);

    // READ
    public List<ClienteTarjeta> obtenerClienteTarjeta(String documento);

    // UPDATE (solamente modificar, no crear). Permite actualizar fecha de expiracion
    public ClienteTarjeta emparcharClienteTarjeta(ClienteTarjeta parche);

}