package com.techu.clientes.servicios.repositorios;

import com.techu.clientes.modelos.Cliente;
import com.techu.clientes.modelos.ClienteTarjeta;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface RepositorioClienteTarjeta extends MongoRepository<ClienteTarjeta, String> {

    public List<ClienteTarjeta> findByDocumento(String documento);

}
