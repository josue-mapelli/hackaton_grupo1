package com.techu.clientes.servicios;

public class ObjetoNoEncontrado extends RuntimeException {
    public ObjetoNoEncontrado(String message) {
        super(message);
    }
}

