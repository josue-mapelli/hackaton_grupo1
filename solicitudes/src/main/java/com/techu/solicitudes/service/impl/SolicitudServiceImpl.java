package com.techu.solicitudes.service.impl;

import com.techu.solicitudes.modelo.Cliente;
import com.techu.solicitudes.modelo.Solicitud;
import com.techu.solicitudes.modelo.Tarjeta;
import com.techu.solicitudes.puente.ConsumoServicioExterno;
import com.techu.solicitudes.repository.SolicitudRepository;
import com.techu.solicitudes.service.SolicitudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Service
public class SolicitudServiceImpl implements SolicitudService {

    @Autowired
    ConsumoServicioExterno consumoServicioExterno;
    @Autowired
    SolicitudRepository solicitudRepository;


    public void validarSolicitudAplazamiento(Solicitud solicitud) {
        //valida existencia de cliente
        final Cliente cliente = consumoServicioExterno.obtenerCliente(solicitud.getDocumento());
        if (Objects.isNull(cliente))
            throw new RuntimeException("No existe el cliente [Documento]: " + solicitud.getDocumento());

        //valida existencia de tarjeta
        final Tarjeta tarjeta = consumoServicioExterno.obtenerTarjeta(solicitud.getDocumento(), solicitud.getPan());
        if (Objects.isNull(cliente))
            throw new RuntimeException("No existe la tarjeta [PAN]: " + tarjeta.getPan());

        solicitud.setEmail(cliente.getCorreo());
        solicitud.getDataAdicional().put("fecha_expiracion_ant", tarjeta.getFechaExpiracion());
        this.validarFechaExpiracion(tarjeta.getFechaExpiracion());
    }

    @Override
    public Solicitud crearSolicitud(Solicitud solicitud) {
        solicitud.setDataAdicional(new HashMap<>());
        solicitud.setFechaSolicitud(this.obtenerFechaHoy());

        solicitud.setEstado("PENDING");

        if ("APLAZAMIENTO_FECHA_EXP".equals(solicitud.getTipoSolicitud())) {
            this.validarSolicitudAplazamiento(solicitud);
        }

        return this.solicitudRepository.save(solicitud);
    }

    @Override
    public Solicitud obtenerSolicitud(String codSolicitud) {
        Solicitud solicitud = this.solicitudRepository.findByCodSolicitud(codSolicitud);
        if (Objects.isNull(solicitud))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        return solicitud;
    }

    @Override
    public void parchearSolicitud(String codSolicitud, Solicitud source) {
        Solicitud target = this.obtenerSolicitud(codSolicitud);

        if ("PENDING".equalsIgnoreCase(target.getEstado())) {
            String nuevaFechaExpiracion = this.obtenerNuevaFechaExpiracion();
            this.consumoServicioExterno.parchearTarjeta(target.getPan(), nuevaFechaExpiracion);

            if (Objects.nonNull(source.getEstado())) {
                target.setEstado(source.getEstado());
            }

            this.consumoServicioExterno.enviarEmail(target);

            target.getDataAdicional().put("fecha_expiracion", nuevaFechaExpiracion);
            this.solicitudRepository.save(target);
        }
    }

    protected void validarFechaExpiracion(String fechaExpiracionTarjeta) {
        try {
            Date fechaExpiracionDate = this.obtenerFormatoFecha().parse(fechaExpiracionTarjeta);

            Date fechaActual = new Date();
            long diff = fechaExpiracionDate.getTime() - fechaActual.getTime();
            TimeUnit time = TimeUnit.DAYS;
            long diferenciaDias = time.convert(diff, TimeUnit.MILLISECONDS);

            if (diferenciaDias > 30)
                throw new RuntimeException("La tarjeta no esta apto para la renovación, tiene más de 30 días de vigencia. [Días de vigencia] :" + diferenciaDias);

        } catch (ParseException ex) {
            throw new RuntimeException("Error no controlado");
        }
    }

    protected String obtenerNuevaFechaExpiracion() {
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.DATE, 720);
        return this.obtenerFormatoFecha().format(c.getTime());
    }

    protected String obtenerFechaHoy() {
        return this.obtenerFormatoFecha().format(new Date());
    }

    protected SimpleDateFormat obtenerFormatoFecha() {
        return new SimpleDateFormat("dd/MM/yyyy");
    }


}
