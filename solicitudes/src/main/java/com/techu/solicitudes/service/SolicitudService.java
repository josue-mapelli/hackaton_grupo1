package com.techu.solicitudes.service;

import com.techu.solicitudes.modelo.Solicitud;

public interface SolicitudService {

    Solicitud crearSolicitud(Solicitud solicitud);

    Solicitud obtenerSolicitud(String codSolicitud);

    void parchearSolicitud(String codSolicitud, Solicitud solicitud);

}
