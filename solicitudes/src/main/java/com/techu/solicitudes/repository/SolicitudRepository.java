package com.techu.solicitudes.repository;

import com.techu.solicitudes.modelo.Solicitud;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SolicitudRepository extends MongoRepository<Solicitud, String> {

    Solicitud findByCodSolicitud(final String codSolicitud);

}
