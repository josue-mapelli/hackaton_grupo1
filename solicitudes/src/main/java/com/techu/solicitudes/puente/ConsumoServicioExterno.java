package com.techu.solicitudes.puente;

import com.techu.solicitudes.modelo.Cliente;
import com.techu.solicitudes.modelo.Email;
import com.techu.solicitudes.modelo.Solicitud;
import com.techu.solicitudes.modelo.Tarjeta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.Map;



@Component
public class ConsumoServicioExterno {
    @Value("${api.externa.obtener-cliente}")
    private String urlObtenerCliente;
    @Value("${api.externa.obtener-oficina}")
    private String urlObtenerOficina;
    @Value("${api.externa.obtener-tarjeta}")
    private String urlObtenerTarjeta;
    @Value("${api.externa.parchear-tarjeta}")
    private String urlParcharTarjeta;
    @Value("${api.externa.enviar-email}")
    private String urlEnviarEmail;
    @Value("${api.externa.enviar-email.asunto}")
    private String emailAsunto;
    @Value("${api.externa.enviar-email.contenido}")
    private String emailContenido;

    @Autowired
    RestTemplate restTemplate;

    public Cliente obtenerCliente(String documento) {
        Map<String, String> params = new HashMap<>();
        params.put("documento", documento);
        final ResponseEntity<Cliente> respuesta = restTemplate.getForEntity(this.urlObtenerCliente, Cliente.class, params);
        if (respuesta.getStatusCode().isError())
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY);

        return respuesta.getBody();
    }

    public Cliente obtenerOficinaGestora(String documento) {
        Map<String, String> params = new HashMap<>();
        params.put("documento", documento);
        final ResponseEntity<Cliente> respuesta = restTemplate.getForEntity(this.urlObtenerCliente, Cliente.class, params);
        if (respuesta.getStatusCode().isError())
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY);

        return respuesta.getBody();
    }

    public Tarjeta obtenerTarjeta(String documento, String tarjeta) {
        Map<String, String> vars = new HashMap<>();
        vars.put("documento", documento);
        vars.put("tarjeta", tarjeta);

        final ResponseEntity<Tarjeta> res = restTemplate.getForEntity(this.urlObtenerTarjeta, Tarjeta.class, vars);

        if (res.getStatusCode().isError()) {
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY);
        }

        return res.getBody();
    }

    public void parchearTarjeta(String pan, String nuevaFecha) {
        Map<String, String> vars = new HashMap<>();
        vars.put("tarjeta", pan);

        Tarjeta tar = new Tarjeta();
        tar.setPan(pan);
        tar.setFechaExpiracion(nuevaFecha);

        HttpEntity<Tarjeta> request = new HttpEntity<>(tar);

        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setConnectTimeout(5000);
        requestFactory.setReadTimeout(5000);

        restTemplate.setRequestFactory(requestFactory);

        final ResponseEntity<Void> res = restTemplate.exchange(this.urlParcharTarjeta, HttpMethod.PATCH, request, Void.class, vars);

        if (res.getStatusCode().isError()) {
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY);
        }
    }

    public void enviarEmail(Solicitud solicitud) {
        Email email = new Email();
        email.setDestinatario(solicitud.getEmail());
        email.setAsunto(emailAsunto);
        String contenido = String.format(emailContenido, solicitud.getPan());
        email.setContenido(contenido);

        HttpEntity<Email> request = new HttpEntity<>(email);

        final ResponseEntity<Void> res = restTemplate.postForEntity(this.urlEnviarEmail, request, Void.class);

        if (res.getStatusCode().isError()) {
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY);
        }

        System.out.printf("Correo enviado a %s, %s, %s", email.getAsunto(), email.getDestinatario(), email.getContenido());
    }


}
