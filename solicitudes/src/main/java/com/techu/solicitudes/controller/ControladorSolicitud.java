package com.techu.solicitudes.controller;

import com.techu.solicitudes.modelo.Solicitud;
import com.techu.solicitudes.service.SolicitudService;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


@RestController
@RequestMapping(Rutas.SOLICITUDES)
public class ControladorSolicitud {

    @Autowired
    SolicitudService servicioSolicitud;

    @PostMapping
    public ResponseEntity crearSolicitud(@RequestBody Solicitud codSolicitud) {
        System.out.println(String.format("obtenerSolicitud %s", codSolicitud));

        try {
            Solicitud solicitud = this.servicioSolicitud.crearSolicitud(codSolicitud);

            final var location = linkTo(
                    methodOn(ControladorSolicitud.class).obtenerSolicitud(solicitud.getCodSolicitud())
            ).toUri();

            return ResponseEntity.created(location).build();
        } catch (RuntimeException re) {
            return new ResponseEntity(re.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{codSolicitud}")
    public ResponseEntity obtenerSolicitud(@PathVariable String codSolicitud) {
        System.out.println(String.format("obtenerSolicitud %s", codSolicitud));
        Solicitud solicitud = this.servicioSolicitud.obtenerSolicitud(codSolicitud);
        return ResponseEntity.ok(solicitud);
    }

    @PatchMapping("/{codSolicitud}")
    public ResponseEntity parchearSolicitud(@PathVariable String codSolicitud, @RequestBody Solicitud solicitud) {
        System.out.println(String.format("parchearSolicitud %s", codSolicitud));

        this.servicioSolicitud.parchearSolicitud(codSolicitud, solicitud);

        return ResponseEntity.noContent().build();
    }

}
