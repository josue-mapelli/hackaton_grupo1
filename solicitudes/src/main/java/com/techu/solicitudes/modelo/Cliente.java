package com.techu.solicitudes.modelo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Cliente {

    private String documento;
    private String nombre;
    private String edad;
    private String fechaNacimiento;
    private String telefono;
    private String correo;
    private String direccion;
}
