package com.techu.solicitudes.modelo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OficinaGestora {
    private String idOficinaGestora;
}
